from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    url(r'^error/$', lambda r: 1/0, name="error"),   
)

urlpatterns += i18n_patterns('',
    url(r'^$', 'i18n_example.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^500/$', 'django.views.defaults.server_error'),
        (r'^404/$', TemplateView.as_view(template_name='404.html')),
    )
